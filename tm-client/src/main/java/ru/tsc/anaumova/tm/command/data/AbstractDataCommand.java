package ru.tsc.anaumova.tm.command.data;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.anaumova.tm.command.AbstractCommand;

@Getter
@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @Autowired
    private IDomainEndpoint domainEndpoint;

}