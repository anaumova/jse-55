package ru.tsc.anaumova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.anaumova.tm.dto.request.DataXmlSaveJaxbRequest;
import ru.tsc.anaumova.tm.enumerated.Role;

@Component
public final class DataXmlSaveJaxbCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Save data to xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        getDomainEndpoint().saveDataXmlJaxb(new DataXmlSaveJaxbRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}